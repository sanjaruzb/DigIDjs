
var btnID;
var divID;
var onSuccessFunction;

var html = '<div style="padding: 15px">\n' +
    '\n' +
    '                        <div class="col-8">\n' +
    '                            <div class="row">\n' +
    '                                <div class="col-5" style="border: 1px solid grey; min-height: 300px">\n' +
    '                                    <p style="margin-top: -10px; background-color: white; margin-left: 20px; width: 97px;padding-left: 10px;">Фотосурати</p>\n' +
    '\n' +
    '                                    <img style="margin: 10px; margin-top: -8px; width: 320px; border: 1px solid #343434" class="PersonPhoto" alt="">\n' +
    '                                </div>\n' +
    '                                <div class="col-6" style=" border: 1px solid grey; min-height: 300px; margin-left: 15px">\n' +
    '                                    <p style="margin-top: -10px; background-color: white; margin-left: 20px; width: 110px;padding-left: 10px;">Малумотлари</p>\n' +
    '                                    <div class="row" style="font-size: 20px">\n' +
    '                                        <div class="col-5">\n' +
    '                                            <p style="line-height: 38px; text-align: right">\n' +
    '                                                Фамилияси <br>\n' +
    '                                                Исми<br>\n' +
    '                                                Тугулган сана<br>\n' +
    '                                                Жинси<br>\n' +
    '                                                Миллати<br>\n' +
    '                                                Серия раками<br>\n' +
    '                                                Амал муддати<br>\n' +
    '                                                Берилган<br>\n' +
    '                                                Шахсий ракам\n' +
    '                                            </p>\n' +
    '                                        </div>\n' +
    '                                        <div class="col-7">\n' +
    '                                            <span class="Surname" style="width: 100%; border: 1px solid grey; display: block;height: 34px;margin: 4px;padding: 2px;margin-top: 0;"></span>\n' +
    '                                            <span class="Name" style="width: 100%; border: 1px solid grey; display: block;height: 34px;margin: 4px;padding: 2px;"></span>\n' +
    '                                            <span class="BirthDate" style="width: 100%; border: 1px solid grey; display: block;height: 34px;margin: 4px;padding: 2px;"></span>\n' +
    '                                            <span class="Sex" style="width: 100%; border: 1px solid grey; display: block;height: 34px;margin: 4px;padding: 2px;"></span>\n' +
    '                                            <span class="IssuerM" style="width: 100%; border: 1px solid grey; display: block;height: 34px;margin: 4px;padding: 2px;"></span>\n' +
    '                                            <span class="DocumentNumber" style="width: 100%; border: 1px solid grey; display: block;height: 34px;padding: 2px;margin: 4px;"></span>\n' +
    '                                            <span class="ExpiryDate" style="width: 100%; border: 1px solid grey; display: block;height: 34px;margin: 4px;padding: 2px;"></span>\n' +
    '                                            <span class="Issuer" style="width: 100%; border: 1px solid grey; display: block;height: 34px;margin: 4px;padding: 2px;"></span>\n' +
    '                                            <span class="Pinpp" style="width: 100%; border: 1px solid grey; display: block;height: 34px;margin: 4px;padding: 2px;"></span>\n' +
    '                                        </div>\n' +
    '                                    </div>\n' +
    '                                    <button class="btn btn-primary col-12 compare">\n' +
    '                                        Compare\n' +
    '                                    </button>\n' +
    '                                </div>\n' +
    '                            </div>\n' +
    '                        </div>\n' +
    '                    </div>'

function DigIDRun(obj){
    console.log(obj)
    if(obj.btnId !== 'undefined'){
        btnID = obj.btnId;
    }

    if(obj.divId !== 'undefined'){
        divID = obj.divId;
    }

    if(obj.onSuccessFunction !== 'undefined'){
        onSuccessFunction = obj.onSuccessFunction;
    }

    temp();
}
function temp(){

    $(document).ready(function () {

        $(document).find(btnID).on( "click", function() {
            console.log('click')
            $(this).attr("disabled", true);
            temp = $(this);
            $.ajax({
                url: 'http://localhost:50000/api/Identification/DataFromReader'
            }).done(function(a) {
                console.log(a)
                temp.removeAttr("disabled");
                var result = $(divID);
                result.append(html);

                result.find('.PersonPhoto').attr('src','data:image/png;base64, '+a.modelPersonPhoto.PersonPhoto )
                result.find('.Surname').html(a.modelPersonPassport.PersonPassport.Surname )
                result.find('.Name').html(a.modelPersonPassport.PersonPassport.Name )
                result.find('.BirthDate').html(a.modelPersonPassport.PersonPassport.BirthDate )
                result.find('.Sex').html(a.modelPersonPassport.PersonPassport.Sex )
                result.find('.IssuerM').html(a.modelPersonPassport.PersonPassport.Issuer )
                result.find('.DocumentNumber').html(a.modelPersonPassport.PersonPassport.DocumentNumber )
                result.find('.ExpiryDate').html(a.modelPersonPassport.PersonPassport.ExpiryDate )
                result.find('.Issuer').html(a.modelPersonPassport.PersonPassport.Issuer )
                result.find('.Pinpp').html(a.modelPersonPassport.PersonPassport.Pinpp )

                result.find('.compare').click(function(){
                    $.ajax({
                        url: 'http://localhost:50000/api/Identification/DataFromForm'
                    }).done(function(a) {
                        console.log(a)
                    });
                })

            });
        });

    })
}
